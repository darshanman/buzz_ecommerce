import React from 'react'
import Brand from './shared/Brand'

const BrandDristibute = () => {
  return (
    <>
      <section>
        <h3>Brands We Distribute</h3>
        <div className='row g-0'>
          <Brand />
          <Brand />
          <Brand />
          <Brand />
          <Brand />
          <Brand />
        </div>
      </section>
    </>
  )
}

export default BrandDristibute
