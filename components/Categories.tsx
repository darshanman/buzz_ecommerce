import Image from 'next/image'
import CategoriesCard from './shared/CategoriesCard'

const Categories = () => {
  return (
    <section>
      <div className='pt-5'>
        <h3 className='mb-5'>Categories </h3>
        <div className='row g-5'>
          <CategoriesCard />
          <CategoriesCard />
          <CategoriesCard />
          <CategoriesCard />
          <CategoriesCard />
          <CategoriesCard />
          <CategoriesCard />
          <CategoriesCard />
          <CategoriesCard />
          <CategoriesCard />
          <CategoriesCard />
          <CategoriesCard />
        </div>
      </div>
    </section>
  )
}

export default Categories
