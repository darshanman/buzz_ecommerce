import DiscountBanner from './shared/DiscountBanner'

const Discount = () => {
  return (
    <div className='row g-5 text-center'>
      <DiscountBanner />
      <DiscountBanner />
      <DiscountBanner />
    </div>
  )
}

export default Discount
