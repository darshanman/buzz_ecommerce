import React from 'react'
import { Button, Col, Container, Form, Row } from 'react-bootstrap'
import FastTruck from './icons/light/FastTruck'

const Footer = () => {
  return (
    <>
      <div className='container mt-5'>
        <div className='row gy-5'>
          <div className='col-6 col-md-3'>
            <h4>
              <FastTruck /> Free Shipping
            </h4>
          </div>
          <div className='col-6 col-md-3'>
            <h4>
              <FastTruck /> Free Shipping
            </h4>
          </div>
          <div className='col-6 col-md-3'>
            <h4>
              <FastTruck /> Free Shipping
            </h4>
          </div>
          <div className='col-6 col-md-3'>
            <h4>
              <FastTruck /> Free Shipping
            </h4>
          </div>
        </div>
      </div>
      <footer className='footer'>
        <Container className='footer-container'>
          <Row className='topFooterSection'>
            <Col md={6} className='topFooterText'>
              <h5 className='footer-topics'>Subscribe to BUZZ</h5>
              <span>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Modi,
                molestiae!
              </span>
            </Col>
            <Col md={6}>
              <div className='footerEmailForm'>
                <Form method='POST'>
                  <Form.Group
                    className='mb-3 footer-group d-flex'
                    controlId='formBasicEmail'
                  >
                    <Form.Control
                      className='footer-form'
                      type='email'
                      required
                      placeholder='Your email address'
                    />
                    <Button
                      className='footer-btn pt-4'
                      variant='none'
                      type='submit'
                    >
                      Submit
                    </Button>
                  </Form.Group>
                </Form>
              </div>
            </Col>
          </Row>
          <Row className='py-5 lowerFooter'>
            <Col md={4}>
              <div className='footer-text'>
                <h3 className='footer-topics'>BUZZ</h3>
                <span>
                  Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                  Quasi, delectus libero! Repellendus repellat accusamus
                  voluptatibus aliquid.
                </span>
                <br />
                <span>
                  Lorem ipsum dolor sit amet consectetur adipisicing elit.
                  Nostrum possimus veniam, facilis cumque eum?
                </span>
              </div>
              <div className='footer-icon'>
                <span className='footer-links p-1'>
                  <a href='#'>Twitter</a>
                </span>
                <span className='footer-links p-1'>
                  <a href='#'>Facebook</a>
                </span>
              </div>
            </Col>
            <Col md={8}>
              <Row>
                <Col md={3}>
                  <h5 className='footer-topics'>Shop and Learn</h5>
                  <ul className='list-unstyled'>
                    <li className='footer-links'>
                      <a href='#!'>Mac</a>
                    </li>
                    <li className='footer-links'>
                      <a href='#!'>iPad</a>
                    </li>
                    <li className='footer-links'>
                      <a href='#!'>iPhone</a>
                    </li>
                    <li className='footer-links'>
                      <a href='#!'>Watch</a>
                    </li>
                    <li className='footer-links'>
                      <a href='#!'>TV</a>
                    </li>
                    <li className='footer-links'>
                      <a href='#!'>Music</a>
                    </li>
                  </ul>
                </Col>
                <Col md={3}>
                  <h5 className='footer-topics'>For Education</h5>
                  <ul className='list-unstyled'>
                    <li className='footer-links'>
                      <a href='#!'>Buzz and Education</a>
                    </li>
                    <li className='footer-links'>
                      <a href='#!'>Shop for College</a>
                    </li>
                  </ul>
                  <h5 className='footer-topics'>For Business</h5>
                  <ul className='list-unstyled'>
                    <li className='footer-links'>
                      <a href='#!'>Buzz and Business</a>
                    </li>
                    <li className='footer-links'>
                      <a href='#!'>Buzz for Business</a>
                    </li>
                  </ul>
                </Col>
                <Col md={3}>
                  <h5 className='footer-topics'>Help</h5>
                  <ul className='list-unstyled'>
                    <li className='footer-links'>
                      <a href='#!'>Track your order</a>
                    </li>
                    <li className='footer-links'>
                      <a href='#!'>Wrranty and Support</a>
                    </li>
                  </ul>
                  <h5 className='footer-topics'>Return Policy</h5>
                  <ul className='list-unstyled'>
                    <li className='footer-links'>
                      <a href='#!'>Service Centers</a>
                    </li>
                    <li className='footer-links'>
                      <a href='#!'>Bulk Orders</a>
                    </li>
                    <li className='footer-links'>
                      <a href='#!'>FAQ's</a>
                    </li>
                    <li className='footer-links'>
                      <a href='#!'>Why buy from us?</a>
                    </li>
                  </ul>
                </Col>
                <Col md={3}>
                  <h5 className='footer-topics'>Company</h5>
                  <ul className='list-unstyled'>
                    <li className='footer-links'>
                      <a href='#!'>About Us</a>
                    </li>
                    <li className='footer-links'>
                      <a href='#!'>Jobs</a>
                    </li>
                    <li className='footer-links'>
                      <a href='#!'>Privacy Policy</a>
                    </li>
                    <li className='footer-links'>
                      <a href='#!'>Terms and Conditions</a>
                    </li>
                    <li className='footer-links'>
                      <a href='#!'>Sitemap</a>
                    </li>
                    <li className='footer-links'>
                      <a href='#!'>Consumer Guarantees</a>
                    </li>
                  </ul>
                </Col>
              </Row>
            </Col>
          </Row>
          <hr className='m-0' />
          <div className='py-4 bottom-footer'>
            <div className='footer-copyright'>
              © 2022 Copyright:
              <a target='_blank' href='https://cagtunepal.com/'>
                {' '}
                cagtunepal.com
              </a>
            </div>
            <div className='paymentLinks m-0 p-0'>
              <ul className='list-unstyled d-flex'>
                <li className='footer-links px-1'>
                  <a href='#!'>About Us</a>
                </li>
                <li className='footer-links px-1'>
                  <a href='#!'>Jobs</a>
                </li>
                <li className='footer-links px-1'>
                  <a href='#!'>Privacy Policy</a>
                </li>
                <li className='footer-links px-1'>
                  <a href='#!'>Terms and Conditions</a>
                </li>
              </ul>
            </div>
          </div>
        </Container>
      </footer>
    </>
  )
}

export default Footer
