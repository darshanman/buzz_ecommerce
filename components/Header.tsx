import { Container, Form, Nav, Navbar } from 'react-bootstrap'

import Location from './icons/regular/Location'
import QuestionCircle from './icons/regular/QuesCircle'
import User from './icons/regular/User'
import Coins from './icons/regular/Coins'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars, faSearch } from '@fortawesome/free-solid-svg-icons'
import { NextRouter, useRouter } from 'next/router'

const Header = () => {
  const handleMenuActive = (path: string, router: NextRouter) => {
    const defaultClass = 'nav-item'
    const activeClass = defaultClass + ' nav-item--active'
    return router.pathname == path ? activeClass : defaultClass
  }
  const router = useRouter()

  return (
    // Site Header Start
    <header id='site-main-header' className='site-main-header'>
      <Container fluid='xl'>
        <Navbar expand='lg'>
          <div className='left-content d-flex align-items-center'>
            <Navbar.Brand href='/'>BUZZ</Navbar.Brand>
            <Nav.Link
              className='location d-none d-sm-inline-block'
              href='#action2'
            >
              <Location /> Store Finder
            </Nav.Link>
          </div>

          <div className='center-content ml-1'>
            <Form className='d-flex my-2 my-lg-0 header-search'>
              <input
                type='text'
                className='header-search__input'
                placeholder=' Search for products'
                name='q'
              />
              <button
                type='submit'
                form='form1'
                className='header-search__button'
              >
                <FontAwesomeIcon icon={faSearch} className='search-icon' />
              </button>
            </Form>
          </div>

          <div className='right-content'>
            <Navbar.Toggle aria-controls='site-navigation' />
            <Navbar.Collapse id='site-navigation'>
              <Nav className='d-flex my-2 my-lg-0 ms-auto navbar-expand-lg static-top right-nav'>
                <Nav.Link href='#action1'>
                  <Coins /> Sell on Buzz!
                </Nav.Link>
                <Nav.Link href='#action1'>
                  <QuestionCircle /> Help &amp; support
                </Nav.Link>
                <Nav.Link href='auth/Login' className='pe-0'>
                  <User /> Login
                </Nav.Link>
                /
                <Nav.Link href='/auth/Register' className='px-0'>
                  Register
                </Nav.Link>
              </Nav>
            </Navbar.Collapse>
          </div>
        </Navbar>
      </Container>
    </header>
    // Site Header End
  )
}

export default Header
