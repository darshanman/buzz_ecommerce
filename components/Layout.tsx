import Head from "next/head";
import { useRouter } from "next/router";
import { FC } from "react";
import meta from "siteMetaData.json";
import { MetaDataProps } from "types/metaData";
import Footer from "./Footer";
import Header from "./Header";
import Navigation from "./Navigation";

const Layout: FC<MetaDataProps> = ({
    children,
    title,
    description,
    keywords,
    ogImage,
    ogUrl,
}) => {
    const buzzOgImage ="";
    return (
        <>
            <Head>
                <title>{!title ? meta.title : title}</title>
                <meta
                    name="description"
                    content={!description ? meta.description : description}
                />
                <meta
                    property="og:title"
                    content={!title ? meta.title : title}
                />
                <meta
                    property="og:description"
                    content={!description ? meta.description : description}
                />
                <meta
                    property="og:url"
                    content={
                        !ogUrl
                            ? ""
                            : `${ogUrl}`
                    }
                />
                <meta
                    property="og:image"
                    content={!ogImage ? buzzOgImage : ogImage}
                />
                <meta
                    name="twitter:url"
                    content={
                        !ogUrl
                            ? ""
                            : `${ogUrl}`
                    }
                />
                <meta
                    name="twitter:title"
                    content={!title ? meta.title : title}
                />
                <meta
                    name="twitter:description"
                    content={!description ? meta.description : description}
                />
                <meta
                    name="keywords"
                    content={!keywords ? meta.keywords : keywords}
                />
                <meta name="robots" content="index, follow" />
            </Head>
            <Header/>
            <Navigation/>
            {children}
            <Footer/>
        </>
    );
};

export default Layout;