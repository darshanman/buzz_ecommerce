import Link from 'next/link'
import React from 'react'
import Image from 'next/image'
import ItemStock from './shared/ItemStock'
import ChevronRight from './icons/regular/ChevronRight'

const Mav = () => {
  return (
    <>
      <div className='d-flex align-items-center justify-content-between multiRow'>
        <h3>Music, Audio, Video</h3>
        <Link href='/products'>
          <a>
            Shop More <ChevronRight />
          </a>
        </Link>
      </div>
      <div className='row g-5'>
        <div className='col-sm-12 col-md-6 col-lg-4'>
          <Link href='/products'>
            <a>
              <Image
                alt='100x100'
                src='/adBanner1.png'
                height={1450}
                width={1550}
              />
            </a>
          </Link>
        </div>
        <div className='col-sm-12 col-md-6 col-lg-8'>
          <div className='row g-5'>
            <ItemStock />
            <ItemStock />
            <ItemStock />
            <ItemStock />
            <ItemStock />
            <ItemStock />
            <ItemStock />
            <ItemStock />
          </div>
        </div>
      </div>
    </>
  )
}

export default Mav
