import Link from 'next/link'
import { Result } from '../types/productTypes'
import ChevronRight from './icons/regular/ChevronRight'
import ItemCard from './shared/ItemCard'

interface ProductProps {
  title: string
  types: Result[]
}

const MultiRow = ({ title, types }: ProductProps) => {
  return (
    <section>
      <div className='d-flex align-items-center justify-content-between multiRow'>
        <h3>{title}</h3>
        <Link href='/products'>
          <a>
            Shop More <ChevronRight />
          </a>
        </Link>
      </div>
      <div className='row g-5'>
        {/* {types?.map((product, key) => {
          return (
            <div key={key} className='col-sm-6 col-lg-3 d-flex'>
              <ItemCard product={product} />
            </div>
          )
        })} */}
      </div>
    </section>
  )
}

export default MultiRow
