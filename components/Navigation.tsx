import { useState } from "react";
import { Container, Nav, Navbar, NavDropdown } from "react-bootstrap";
import ChevronDown from "./icons/regular/ChevronDown";
import HamburgerMenu from "./icons/regular/HamburgerMenu";
import Heart from "./icons/regular/Heart";
import ShoppingBag from "./icons/regular/ShopingBag";

import { useAppSelector } from "../redux/hooks";
import Link from "next/link";
import CartFly from "./cart/CartFly";



const Navigation = () => {

  const [cartOpen, setCartOpen] = useState<boolean>(false);

  const { numOfCarts } = useAppSelector((state) => state.cart);

  return (
    <section className='site-navigation'>
      <Container className='text-light'>
      <CartFly cartOpen={cartOpen} setCartOpen={setCartOpen} />
        <Navbar
          collapseOnSelect
          expand='lg'
          className='p-0 d-flex justify-content-between'
          sticky='top'
        >
          <div className="left-content">
          <NavDropdown
                title={
                  <>
                    <HamburgerMenu /> shop by categories
                  </>
                }
                id='collasible-nav-dropdown'
                renderMenuOnMount
              >
                <NavDropdown.Item href='/'>Action</NavDropdown.Item>
                <NavDropdown.Item href='/'>Another action</NavDropdown.Item>
                <NavDropdown.Item href='/'>Something</NavDropdown.Item>
              </NavDropdown>
          </div>
          <div className="center-content">
          <Navbar.Toggle aria-controls='responsive-navbar-nav' />
            <Navbar.Collapse id='responsive-navbar-nav'>
              <Nav className='d-flex mx-auto'>
                <NavDropdown
                  title={
                    <span>
                      Products
                      <span className='mx-3'>
                      <ChevronDown />
                      </span>
                    </span>
                    
                  }
                  id='collasible-nav-dropdown'
                  className='mx-4'
                >
                  <NavDropdown.Item href='#action/3.1'>Action</NavDropdown.Item>
                  <NavDropdown.Item href='#action/3.2'>
                    Another action
                  </NavDropdown.Item>
                  <NavDropdown.Item href='#action/3.3'>Something</NavDropdown.Item>
                </NavDropdown>
                <NavDropdown
                  title={
                    <span>
                      Brands
                      <span className='mx-3'>
                      <ChevronDown />
                      </span>
                    </span>
                  }
                  id='collasible-nav-dropdown'
                  className='mx-4'
                >
                  <NavDropdown.Item href='#action/3.1'>Action</NavDropdown.Item>
                  <NavDropdown.Item href='#action/3.2'>
                    Another action
                  </NavDropdown.Item>
                  <NavDropdown.Item href='#action/3.3'>Something</NavDropdown.Item>
                </NavDropdown>
                <NavDropdown
                  title={
                    <span>
                      Deals &amp; sales
                      <span className='mx-3'><ChevronDown/></span>
                    </span>
                  }
                  id='collasible-nav-dropdown'
                  className='mx-4'
                >
                  <NavDropdown.Item href='#action/3.1'>Action</NavDropdown.Item>
                  <NavDropdown.Item href='#action/3.2'>
                    Another action
                  </NavDropdown.Item>
                  <NavDropdown.Item href='#action/3.3'>Something</NavDropdown.Item>
                </NavDropdown>
                <NavDropdown
                  title={
                    <span>
                      Services
                      <span className='mx-3'><ChevronDown/></span>
                    </span>
                  }
                  id='collasible-nav-dropdown'
                  className='mx-4'
                >
                  <NavDropdown.Item href='#action/3.1'>Action</NavDropdown.Item>
                  <NavDropdown.Item href='#action/3.2'>
                    Another action
                  </NavDropdown.Item>
                  <NavDropdown.Item href='#action/3.3'>Something</NavDropdown.Item>
                </NavDropdown>
                <NavDropdown
                  title={
                    <span>
                      Gift Cards
                      <span className='mx-3'><ChevronDown/></span>
                    </span>
                  }
                  id='collasible-nav-dropdown'
                  className='mx-4'
                >
                  <NavDropdown.Item href='#action/3.1'>Action</NavDropdown.Item>
                  <NavDropdown.Item href='#action/3.2'>
                    Another action
                  </NavDropdown.Item>
                  <NavDropdown.Item href='#action/3.3'>Something</NavDropdown.Item>
                </NavDropdown>
              </Nav>
              
            </Navbar.Collapse>
          </div>
          <div className="right-content">
            <Nav className='ms-auto ps-3'>
              <Link href='/'>
                <Heart />
              </Link>
              <Link href='#!'>
                <a className='cart'>
                  <span onClick={() => setCartOpen(!cartOpen)}><ShoppingBag/></span>
                  <span className='badge'>{numOfCarts}</span>
                </a>
              </Link>
            </Nav>
          </div>
            
        </Navbar>
      </Container>
    </section>
  )
}

export default Navigation;
