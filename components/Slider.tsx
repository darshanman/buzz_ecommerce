import Link from 'next/link'
import Slider from 'react-slick'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import { Container } from 'react-bootstrap'
import Image from 'next/image'
import banner1 from '../public/topBanner.png'
import banner2 from '../public/banner2.png'

const FeatureSlider = () => {
  var settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
  }

  return (
    <>
      <div className='slider-container'>
        <Slider {...settings}>
          <div className='slides d-flex'>
            <Link key={''} passHref href='/'>
              <Image src={banner1} />
            </Link>
          </div>
          <div className='slides d-flex'>
            <Link key={''} passHref href='/'>
              <Image src={banner1} />
            </Link>
          </div>
          <div className='slides d-flex'>
            <Link key={''} passHref href='/'>
              <Image src={banner2} />
            </Link>
          </div>
        </Slider>
      </div>
    </>
  )
}

export default FeatureSlider
