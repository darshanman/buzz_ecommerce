import Link from 'next/link'
import React from 'react'
import ChevronRight from './icons/regular/ChevronRight'
import Image from 'next/image'
import ItemCard from './shared/ItemCard'
import { Result } from '../types/productTypes'

interface ProductProps {
  types: Result[]
}
const SpecialDeals = ({ types }: ProductProps) => {
  return (
    <section>
      <div className='d-flex align-items-center justify-content-between multiRow mt-0'>
        <h3>Special Deals</h3>
        <Link href='/products'>
          <a>
            Shop More <ChevronRight />
          </a>
        </Link>
      </div>
      <div className='row g-5'>
        <div className='col-sm-12 col-md-6 col-lg-3'>
          <Link href='/products'>
            <a>
              <Image
                alt='100x100'
                src='/adBanner1.png'
                height={1450}
                width={1550}
              />
            </a>
          </Link>
        </div>
        <div className='col-sm-12 col-md-6 col-lg-9'>
          <div className='row g-5'>
            {types?.slice(0, 3)?.map((product, key) => {
              return (
                <div key={key} className='col-sm-6 col-md-4 d-flex'>
                  <ItemCard product={product} />
                </div>
              )
            })}  */}
          </div>
        </div>
      </div>
    </section>
  )
}

export default SpecialDeals
