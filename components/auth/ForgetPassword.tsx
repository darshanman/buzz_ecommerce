import { Form, Formik } from "formik";
import React from "react";
import { forgetPasswordFormData } from "../../utils/formData";
import forgetPasswordFormSchema from "../../utils/formValidation/forgetPasswordFormValidation";
import FormButton from "../common/FormButton";
import InputField from "../common/InputField";

const ForgetPassword = () => {
  const submitHandler = () => {
    console.log("works");
  };
  return (
    <>
      <div className="forget-password-text">
        Enter the email associated with your account and we’ll send an email
        with instructions to reset your password.
      </div>
      <Formik
        initialValues={forgetPasswordFormData}
        validationSchema={forgetPasswordFormSchema}
        onSubmit={submitHandler}
      >
        {({ errors, touched }) => (
          <Form className="login-form">
            <InputField
              type="email"
              name="email"
              labelName="Email"
              touch={touched.email}
              error={errors.email}
              placeHolder="Enter your email address"
            />
            <FormButton name={"Send"}></FormButton>
          </Form>
        )}
      </Formik>
    </>
  );
};
export default ForgetPassword;
