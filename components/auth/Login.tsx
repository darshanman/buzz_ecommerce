import { Form, Formik } from 'formik'
import { Row, Col } from 'react-bootstrap'
import React from 'react'
import { loginFormData } from '../../utils/formData'
import loginFormSchema from '../../utils/formValidation/loginFormValidation'
import FormButton from '../common/FormButton'
import InputField from '../common/InputField'
import PasswordField from '../common/PasswordField'
import GoogleButton from '../common/GoogleLogin'
import http from '../../pages/api/httpService'
import urls from '../../pages/constant/urls'

const Login = () => {
  return (
    <div>
      <Formik
        initialValues={loginFormData}
        validationSchema={loginFormSchema}
        onSubmit={async (values, actions) => {
          try {
            const response = await http.post(`${urls.auth.login}`, values)
            if (response.data.error)
              throw new Error(response.data.error.message)
            else {
              console.log(values)
              actions.setSubmitting(false)
            }
          } catch (ex: any) {
            if (ex.response && ex.response.status === 404) {
              console.log('error')
              actions.resetForm()
            } else {
              console.log('error')
              actions.resetForm()
            }
            actions.setSubmitting(false)
          }
        }}
      >
        {({ errors, touched }) => (
          <Form className='login-form'>
            <InputField
              type='email'
              name='email'
              labelName='Email'
              touch={touched.email}
              error={errors.email}
              placeHolder='example@example.com'
            />
            <PasswordField
              type='password'
              name='password'
              labelName='Password'
              touch={touched.password}
              error={errors.password}
              placeHolder='xxxxxxxxxxxxxx'
              forgetPass='Forget Password?'
            />
            <FormButton name={'Login'}></FormButton>

            <div className='horLine'>
              <span>OR</span>
            </div>

            <GoogleButton name={'Sign in with Google'}></GoogleButton>
          </Form>
        )}
      </Formik>
    </div>
  )
}
export default Login
