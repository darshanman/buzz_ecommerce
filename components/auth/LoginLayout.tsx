import React, { ReactNode } from "react";

interface LayoutProps {
  topLeftText: string;
  children: ReactNode;
  topRightText: string;
  welcomeText: string;
  headerText: string;
  link: string;
}

const LoginLayout = ({
  children,
  topLeftText,
  topRightText,
  welcomeText,
  headerText,
  link,
}: LayoutProps) => {
  return (
    <div className="login-layout__wrapper">
      <div
        className="login-page__left"
        style={{
          backgroundImage: `url("/loginBanner.png")`,
        }}
      ></div>
      <div className="login-page__right">
        <div className="top-text">
          {topLeftText}{" "}
          <a href={link} className="create-link">
            {topRightText}
          </a>
        </div>
        <div className="login-total__wrapper">
          <div className="login-bottom">
            <div className="login__wrapper">
              <span className="welcome-text">{welcomeText}</span>
              <br />
              <div className="login-text">{headerText}</div>
              {children}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default LoginLayout;
