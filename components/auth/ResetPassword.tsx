import { Form, Formik } from "formik";
import React from "react";
import { resetPasswordFormData } from "../../utils/formData";
import resetPasswordFormSchema from "../../utils/formValidation/forgetPasswordFormValidation";
import FormButton from "../common/FormButton";
import PasswordField from "../common/PasswordField";

const ResetPassword = () => {
  const submitHandler = () => {
    console.log("works");
  };
  return (
    <>
      <div className="forget-password-text">
        Your new password must be different from your previous used passwords.
      </div>
      <Formik
        initialValues={resetPasswordFormData}
        validationSchema={resetPasswordFormSchema}
        onSubmit={submitHandler}
      >
        {({ errors, touched }) => (
          <Form className="login-form">
            <PasswordField
              type="password"
              name="password"
              labelName="Password"
              touch={touched.password}
              error={errors.password}
              placeHolder="xxxxxxxxxxx"

            />
            <PasswordField
              type="password"
              name="confirmPassword"
              labelName="Confirm Password"
              touch={touched.password}
              error={errors.password}
              placeHolder="xxxxxxxxxxx"
            />
            <FormButton name={"Reset Password"}></FormButton>
          </Form>
        )}
      </Formik>
    </>
  );
};
export default ResetPassword;
