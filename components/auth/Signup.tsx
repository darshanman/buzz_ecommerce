import { Form, Formik } from 'formik'
import React from 'react'
import { signupFormData } from '../../utils/formData'
import signupFormSchema from '../../utils/formValidation/signupFormValidation'
import FormButton from '../common/FormButton'
import InputField from '../common/InputField'
import PasswordField from '../common/PasswordField'
import GoogleButton from '../common/GoogleLogin'
import http from '../../pages/api/httpService'
import urls from '../../pages/constant/urls'

const Signup = () => {
  return (
    <Formik
      initialValues={signupFormData}
      validationSchema={signupFormSchema}
      onSubmit={async (values, actions) => {
        try {
          const response = await http.post(`${urls.auth.signup}`, values)
          if (response.data.error) throw new Error(response.data.error.message)
          else {
            console.log(values)
            actions.setSubmitting(false)
          }
        } catch (ex: any) {
          if (ex.response && ex.response.status === 404) {
            console.log('error 1')
          } else {
            console.log('error 2')
          }
          actions.setSubmitting(false)
        }
      }}
    >
      {({ errors, touched }) => (
        <Form className='login-form'>
          <InputField
            type='text'
            name='username'
            labelName='username'
            touch={touched.username}
            error={errors.username}
            placeHolder='Enter your username'
          />
          <InputField
            type='text'
            name='first_name'
            labelName='First Name'
            touch={touched.first_name}
            error={errors.first_name}
            placeHolder='Enter your First Name'
          />
          <InputField
            type='text'
            name='last_name'
            labelName='Last Name'
            touch={touched.last_name}
            error={errors.last_name}
            placeHolder='Enter your Last Name'
          />
          <InputField
            type='email'
            name='email'
            labelName='Email'
            touch={touched.email}
            error={errors.email}
            placeHolder='example@example.com'
          />
          <InputField
            type='text'
            name='phone'
            labelName='Phone'
            touch={touched.phone}
            error={errors.phone}
            placeHolder='e.g. +977 - 98234163458'
          />
          <PasswordField
            type='password'
            name='password'
            labelName='Password'
            touch={touched.password}
            error={errors.password}
            placeHolder='xxxxxxxxxxxxxx'
          />
          <PasswordField
            type='password'
            name='confirm_password'
            labelName='Confirm Password'
            touch={touched.confirm_password}
            error={errors.confirm_password}
            placeHolder='xxxxxxxxxxxxxx'
          />
          <FormButton type='submit' name={'Register'}></FormButton>

          <div className='horLine'>
            <span>OR</span>
          </div>

          <GoogleButton name={'Sign up with Google'}></GoogleButton>
        </Form>
      )}
    </Formik>
  )
}
export default Signup
