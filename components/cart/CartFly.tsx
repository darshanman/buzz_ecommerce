import React, { useState } from "react";
import { ProgressBar } from "react-bootstrap";
import FireFlame from "../icons/regular/FireFlame";
import Image from "next/image";
import QuantityButton from "../common/QuantityButton";
import Trash from "../icons/regular/Trash";
import CouponSection from "../common/CouponSection";
import CartSummary from "./CartSummary";
import Xmark from "../icons/regular/Xmark";
import { boolean } from "yup";

interface cartProps {
  cartOpen: boolean;
  setCartOpen: React.Dispatch<React.SetStateAction<boolean>>;
}

const CartFly = ({ cartOpen, setCartOpen }: cartProps) => {
  const [quantity, setQuantity] = useState(1);

  return (
    <>
      <div
        className={`${
          cartOpen ? "cartfly__wrapper active" : "cartfly__wrapper"
        }`}
      >
        <div className="upper-section">
          <div className="top-section">
            <div className="shopping-and-icon d-flex">
              <h3>Shopping Cart</h3>
              <span className="icon" onClick={() => setCartOpen(false)}>
                <Xmark />
              </span>
            </div>
            <span className="fire-icon">
              <FireFlame />
            </span>
            <span className="warning-text">
              These products are limited, checkout soon
            </span>
            <div className="congrats-text">
              Congrats ! You are eligible for <strong>FREE Shipping</strong>
            </div>
            <ProgressBar variant="warning" now={100} />
          </div>

          <div className="items-section">
            {/*each items in cart*/}

            <div className="total-items__wrapper">
              <div className="each-item">
                <div className="img-section">
                  <Image
                    src="/d3.png"
                    alt="Picture of the author"
                    width={88}
                    height={88}
                  />
                </div>
                <div className="desc-section">
                  <div className="title">The Brushed Flannel Caro Shirt</div>
                  <span className="price">$48.00</span>
                  <div className="attributes">
                    <div className="quantityButton">
                      <button onClick={() => setQuantity(quantity - 1)}>
                        -
                      </button>
                      <span className="number">{quantity}</span>
                      <button onClick={() => setQuantity(quantity + 1)}>
                        +
                      </button>
                    </div>

                    <span className="delete">
                      <Trash />
                    </span>
                  </div>
                </div>
              </div>
              <hr />
            </div>

            <div className="total-items__wrapper">
              <div className="each-item">
                <div className="img-section">
                  {/* <img src="/d3.png" alt="shoe" /> */}
                  <Image
                    src="/d3.png"
                    alt="Picture of the author"
                    width={88}
                    height={88}
                  />
                </div>
                <div className="desc-section">
                  <div className="title">The Brushed Flannel Caro Shirt</div>
                  <span className="price">$48.00</span>
                  <div className="attributes">
                    <div className="quantityButton">
                      <button onClick={() => setQuantity(quantity - 1)}>
                        -
                      </button>
                      <span className="number">{quantity}</span>
                      <button onClick={() => setQuantity(quantity + 1)}>
                        +
                      </button>
                    </div>
                    <span className="delete">
                      <Trash />
                    </span>
                  </div>
                </div>
              </div>
              <hr />
            </div>

            <div className="total-items__wrapper">
              <div className="each-item">
                <div className="img-section">
                  {/* <img src="/d3.png" alt="shoe" /> */}
                  <Image
                    src="/d3.png"
                    alt="Picture of the author"
                    width={88}
                    height={88}
                  />
                </div>
                <div className="desc-section">
                  <div className="title">The Brushed Flannel Caro Shirt</div>
                  <span className="price">$48.00</span>
                  <div className="attributes">
                    <div className="quantityButton">
                      <button>-</button>
                      <span className="number">1</span>
                      <button>+</button>
                    </div>

                    <span className="delete">
                      <Trash />
                    </span>
                  </div>
                </div>
              </div>
              <hr />
            </div>
          </div>
        </div>
        {/* footers section */}
        <div className="footer-section">
          <div className="coupon-section">
            <CouponSection />
          </div>
          <div className="checkout-section">
            <CartSummary 
            isLinkText={true}/>
            {/* <div className="view-carts">
              <a href="/carts">View Cart</a>
            </div> */}
          </div>
        </div>
      </div>
    </>
  );
};

export default CartFly;
