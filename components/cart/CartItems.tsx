import React, {useState} from "react";
import Trash from "../icons/regular/Trash";
import RightArrow from "../icons/regular/RightArrow";
import CartSummary from "./CartSummary";
import Image from "next/image";
import rect from "../../public/r1.png";
import CouponSection from "../common/CouponSection";

function CartItems() {

  return (
    <div className='items_container'>
      <div className='table-responsive'>
        <table className='table'>
          <thead>
            <tr>
              <th className='products'>Product</th>
              <th className='price'>Price</th>
              <th className='quantity'>Quantity</th>
              <th className='subtotal'>Subtotal</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td className='products'>
                <div className='cart-col1 d-flex'>
                  <div className='item-image'>
                    <Image src={rect} alt='item image' height={65} width={65} />
                  </div>
                  <div className='item-desc'>
                    <div className='item-title'>
                      Sony WH-1000XM4 Wireless Noise Cancelling Over-Ear
                      Headphones (Black)
                    </div>
                    <div className='item-attributes'>
                      <Trash />

                      <a className='remove'>Remove</a>
                    </div>
                  </div>
                </div>
              </td>

              <td className='price'>
                <span className='pricing'>$5000.00</span>
              </td>

              <td className='quantity'>
                <QuantityButton />
              </td>

              <td className='subtotal'>$5000.00</td>
            </tr>

            <tr>
              <td className='products'>
                <div className='cart-col1 d-flex'>
                  <div className='item-image'>
                    <Image src={rect} alt='item image' height={65} width={65} />
                  </div>
                  <div className='item-desc'>
                    <div className='item-title'>
                      Sony WH-1000XM4 Wireless Noise Cancelling Over-Ear
                      Headphones (Black)
                    </div>
                    <div className='item-attributes'>
                      <Trash />

                      <a className='remove'>Remove</a>
                    </div>
                  </div>
                </div>
              </td>

              <td className='price'>
                <span className='pricing'>$5000.00</span>
              </td>

              <td className='quantity'>quantiy input</td>

              <td className='subtotal'>$5000.00</td>
            </tr>
          </tbody>
        </table>
      </div>

      <div className="cart_footer d-flex">
        <div className="coupon_section">
          <CouponSection />
        </div>
        <div className='checkout_section'>
          <CartSummary isLinkText={false} />
        </div>
      </div>
    </div>
  )
}

export default CartItems
