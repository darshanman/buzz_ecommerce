import Link from "next/link";
import React from "react";

interface cartProps {
  isLinkText: boolean;
}

function CartSummary(isLinkText: cartProps) {
  return (
    <div className='cart_summary_wrapper'>
      <div className='outline'>
        <div className='content'>
          <div className='discount d-flex'>
            <div className='left'>Discount</div>
            <div className='right'>$250.00</div>
          </div>
          <hr />
          <div className='discount d-flex'>
            <div className='left'>Shipping</div>
            <div className='right'>FreeShipping</div>
          </div>
          <hr />
          <div className='discount d-flex'>
            <div className='left'>Total</div>
            <div className='right'>$14325.00</div>
          </div>
          <button className="checkout_button">Checkout</button>
          {isLinkText.isLinkText &&
          <div className="view-carts">
              <Link href="/carts">
                <a>View Cart</a>
              </Link>
            </div>
          }
        </div>
      </div>
    </div>
  )
}

export default CartSummary;
