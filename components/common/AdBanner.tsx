import React from 'react'
import ad1 from '../../public/ad1.png'
import Image from 'next/image'
import Link from 'next/link'

const AdBanner = () => {
  return (
    <div className='py-5'>
      <Link href='/products'>
        <a>
          <Image src={ad1} alt='Card image cap' />
        </a>
      </Link>
    </div>
  )
}

export default AdBanner
