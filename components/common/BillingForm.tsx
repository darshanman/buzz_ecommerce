import { Field, Form, Formik } from 'formik'
import React, { useState } from 'react'
import http from '../../pages/api/httpService'
import urls from '../../pages/constant/urls'
import { billingFormData } from '../../utils/formData'
import billingFormSchema from '../../utils/formValidation/billingFormValidation'
import FormButton from './FormButton'
import InputField from './InputField'
import { CustomSelect } from '../shared/SelectField'

const BillingForm = () => {
  const [checked, setChecked] = useState(false)

  const handleChange = () => {
    setChecked(!checked)
  }
  const countryOptions = [
    {
      label: 'Nepal',
      value: 'nepal',
    },
    {
      label: 'USA',
      value: 'usa',
    },
    {
      label: 'Australia',
      value: 'australia',
    },
    {
      label: 'France',
      value: 'france',
    },
    {
      label: 'Spain',
      value: 'spain',
    },
  ]
  return (
    <div className='row'>
      <Formik
        initialValues={billingFormData}
        validationSchema={billingFormSchema}
        onSubmit={async (values, actions) => {
          try {
            const response = await http.post(`${urls.auth.login}`, values)
            if (response.data.error)
              throw new Error(response.data.error.message)
            else {
              console.log(values)
              actions.setSubmitting(false)
            }
          } catch (ex: any) {
            if (ex.response && ex.response.status === 404) {
              console.log('error')
              actions.resetForm()
            } else {
              console.log('error')
              actions.resetForm()
            }
            actions.setSubmitting(false)
          }
        }}
      >
        {({ errors, touched }) => (
          <>
            <Form>
              <section>
                <h2>Billing Details</h2>
                <div className='row gx-5'>
                  <div className='col-sm-12 col-md-6'>
                    <InputField
                      type='text'
                      name='first_name'
                      labelName='First Name'
                      touch={touched.first_name}
                      error={errors.first_name}
                      fieldRequired={true}
                      placeHolder='Enter your first name'
                    />
                  </div>
                  <div className='col-sm-12 col-md-6'>
                    <InputField
                      type='text'
                      name='last_name'
                      labelName='Last Name'
                      touch={touched.last_name}
                      error={errors.last_name}
                      fieldRequired={true}
                      placeHolder='Enter your last name'
                    />
                  </div>
                </div>
                <Field
                  name='country'
                  labelName='Country'
                  options={countryOptions}
                  component={CustomSelect}
                  touch={touched.last_name}
                  error={errors.last_name}
                  fieldRequired={true}
                  placeholder='Select a Country...'
                  isMulti={false}
                />
                <div className='row gx-5'>
                  <div className='col-sm-12 col-md-6'>
                    <InputField
                      type='text'
                      name='state'
                      labelName='State'
                      touch={touched.state}
                      error={errors.state}
                      fieldRequired={true}
                      placeHolder='Enter your state'
                    />
                  </div>
                  <div className='col-sm-12 col-md-6'>
                    <InputField
                      type='text'
                      name='city'
                      labelName='City'
                      touch={touched.city}
                      error={errors.city}
                      fieldRequired={true}
                      placeHolder='Enter your city'
                    />
                  </div>
                </div>
                <div className='row gx-5'>
                  <div className='col-sm-12 col-md-6'>
                    <InputField
                      type='text'
                      name='street_address'
                      labelName='Street Address'
                      touch={touched.street_address}
                      error={errors.street_address}
                      fieldRequired={true}
                      placeHolder='Enter your street address'
                    />
                  </div>
                  <div className='col-sm-12 col-md-6'>
                    <InputField
                      type='number'
                      name='postal_code'
                      labelName='Postal Code'
                      touch={touched.postal_code}
                      error={errors.postal_code}
                      fieldRequired={true}
                      placeHolder='Enter your postal code'
                    />
                  </div>
                  <div className='col-sm-12 col-md-6'>
                    <InputField
                      type='email'
                      name='email'
                      labelName='Email Address'
                      touch={touched.email}
                      error={errors.email}
                      fieldRequired={true}
                      placeHolder='Enter your Email address'
                    />
                  </div>
                  <div className='col-sm-12 col-md-6'>
                    <InputField
                      type='text'
                      name='phone'
                      labelName='Contact Number'
                      touch={touched.phone}
                      error={errors.phone}
                      fieldRequired={true}
                      placeHolder='Enter your Contact Number'
                    />
                  </div>
                </div>
              </section>
              <div className='billing__checkbox'>
                <input
                  type='checkbox'
                  checked={checked}
                  onChange={handleChange}
                />{' '}
                Ship to a different address?
              </div>

              {checked && (
                <section className='mt-5'>
                  <h2>Shipping Details</h2>
                  <div className='row gx-5'>
                    <div className='col-sm-12 col-md-6'>
                      <InputField
                        type='text'
                        name='first_name'
                        labelName='First Name'
                        touch={touched.first_name}
                        error={errors.first_name}
                        fieldRequired={true}
                        placeHolder='Enter your first name'
                      />
                    </div>
                    <div className='col-sm-12 col-md-6'>
                      <InputField
                        type='text'
                        name='last_name'
                        labelName='Last Name'
                        touch={touched.last_name}
                        error={errors.last_name}
                        fieldRequired={true}
                        placeHolder='Enter your last name'
                      />
                    </div>
                  </div>
                  <Field
                    name='country'
                    labelName='Country'
                    options={countryOptions}
                    component={CustomSelect}
                    touch={touched.last_name}
                    error={errors.last_name}
                    fieldRequired={true}
                    placeholder='Select a Country...'
                    isMulti={false}
                  />
                  <div className='row gx-5'>
                    <div className='col-sm-12 col-md-6'>
                      <InputField
                        type='text'
                        name='state'
                        labelName='State'
                        touch={touched.state}
                        error={errors.state}
                        fieldRequired={true}
                        placeHolder='Enter your state'
                      />
                    </div>
                    <div className='col-sm-12 col-md-6'>
                      <InputField
                        type='text'
                        name='city'
                        labelName='City'
                        touch={touched.city}
                        error={errors.city}
                        fieldRequired={true}
                        placeHolder='Enter your city'
                      />
                    </div>
                  </div>
                  <div className='row gx-5'>
                    <div className='col-sm-12 col-md-6'>
                      <InputField
                        type='text'
                        name='street_address'
                        labelName='Street Address'
                        touch={touched.street_address}
                        error={errors.street_address}
                        fieldRequired={true}
                        placeHolder='Enter your street address'
                      />
                    </div>
                    <div className='col-sm-12 col-md-6'>
                      <InputField
                        type='number'
                        name='postal_code'
                        labelName='Postal Code'
                        touch={touched.postal_code}
                        error={errors.postal_code}
                        fieldRequired={true}
                        placeHolder='Enter your postal code'
                      />
                    </div>
                  </div>
                  <div className='row gx-5'>
                    <div className='col-sm-12 col-md-6'>
                      <InputField
                        type='email'
                        name='email'
                        labelName='Email Address'
                        touch={touched.email}
                        error={errors.email}
                        fieldRequired={true}
                        placeHolder='Enter your Email address'
                      />
                    </div>
                    <div className='col-sm-12 col-md-6'>
                      <InputField
                        type='text'
                        name='phone'
                        labelName='Contact Number'
                        touch={touched.phone}
                        error={errors.phone}
                        fieldRequired={true}
                        placeHolder='Enter your Contact Number'
                      />
                    </div>
                  </div>
                </section>
              )}
              <FormButton
                type='submit'
                name={'Continue To Payment'}
                className='secondary'
              ></FormButton>
            </Form>
          </>
        )}
      </Formik>
    </div>
  )
}

export default BillingForm
