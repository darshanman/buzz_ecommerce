import React from 'react'
import RightArrow from '../icons/regular/RightArrow';

const CouponSection = () => {
  return (
    <div className="coupon-component">
      <span className="coupon_text">
        If you have a coupon code, please apply it below.
      </span>

      <div className="input_wrapper">
        <input
          type="text"
          className="coupon_input"
          placeholder="Coupon Code"
          name="q"
        />
        <button
          type="submit"
          form="form1"
          className="topHeader__search__button"
        >
          <RightArrow />
        </button>
      </div>
    </div>
  );
}

export default CouponSection;
