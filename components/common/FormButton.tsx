import { ButtonHTMLAttributes, DetailedHTMLProps } from 'react'
import { FormButtonProps } from '../../types/formButton'

const FormButton = ({
  name,
  className,
  ...restProps
}: FormButtonProps &
  Partial<
    DetailedHTMLProps<
      ButtonHTMLAttributes<HTMLButtonElement>,
      HTMLButtonElement
    >
  >) => {
  return (
    <button {...restProps} className={`barButton ${className}`}>
      <span>{name}</span>
    </button>
  )
}

export default FormButton
