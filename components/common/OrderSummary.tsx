import React from 'react'
import Image from 'next/image'
import rect from '../../public/d3.png'

const OrderSummary = () => {
  return (
    <div className='orderSummary'>
      <h3>Order Summary</h3>
      <div className='d-flex'>
        <div className='orderSummary__image'>
          <Image src={rect} alt='item image' width={80} height={80} />
        </div>
        <div className='orderSummary__desc'>
          <h5>
            Sony WH-1000XM4 Wireless Noise Cancelling Over-Ear Headphones
            (Black)
          </h5>
          <span>$100</span>
        </div>
      </div>
      <hr />
      <div className='orderSummary__coupon'>
        If you have a coupon code, please apply it below.
      </div>
      <hr />
      <div className='d-flex justify-content-between align-items-center text-center'>
        <h5>Sub Total</h5>
        <span>$100</span>
      </div>
      <div className='d-flex justify-content-between pt-3'>
        <h5>Shipping</h5>
        <span>Calculated at next step</span>
      </div>
      <hr />
      <div className='d-flex justify-content-between align-items-center text-center orderSummary__total'>
        <h4>Order Total</h4>
        $100
      </div>
    </div>
  )
}

export default OrderSummary
