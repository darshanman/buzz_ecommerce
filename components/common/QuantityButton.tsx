import React from 'react'

const QuantityButton = () => {
  return (
    <div className='quantityButton'>
      <button>-</button> 1 <button>+</button>
    </div>
  )
}

export default QuantityButton
