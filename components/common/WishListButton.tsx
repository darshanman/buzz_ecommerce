import React from 'react'
import Heart from '../icons/regular/Heart'

const WishListButton = () => {
  return (
    <div className='wishListButton'>
      <Heart />
    </div>
  )
}

export default WishListButton
