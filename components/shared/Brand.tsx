import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faApple } from '@fortawesome/free-brands-svg-icons'

const Brand = () => {
  return (
    <div className='col-6 col-md-4 col-lg-2'>
      <div className='item-card text-center py-4'>
        <FontAwesomeIcon icon={faApple} width={28} height={28} />
      </div>
    </div>
  )
}

export default Brand
