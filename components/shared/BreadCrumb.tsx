import React from 'react'
import { Breadcrumb } from 'react-bootstrap'

const BreadCrumb = () => {
  return (
    <section>
      <div className='container'>
        <Breadcrumb>
          <Breadcrumb.Item href='#'>Home</Breadcrumb.Item>
          <Breadcrumb.Item href=''>Library</Breadcrumb.Item>
          <Breadcrumb.Item active>Data</Breadcrumb.Item>
        </Breadcrumb>
      </div>
    </section>
  )
}

export default BreadCrumb
