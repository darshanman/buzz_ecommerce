import Image from 'next/image'
import Link from 'next/link'
import Radio from '../../public/r2.png'

const CategoriesCard = () => {
  return (
    <div className='col-6 col-md-3 col-lg-2 my-2'>
      <Link href='/category/1'>
        <a>
          <div className='categories text-center pt-3'>
            <div className='categories__image d-flex text-center mx-auto'>
              <Image src={Radio} alt='Card image cap' height={65} width={65} />
            </div>
            <h5 className='py-5'>TV & Audio</h5>
          </div>
        </a>
      </Link>
    </div>
  )
}

export default CategoriesCard
