import React from 'react'
import Image from 'next/image'
import d1 from '../../public/dis1.png'
import Link from 'next/link'

const DiscountBanner = () => {
  return (
    <div className='col-md-4'>
      <Link href='/products'>
        <a>
          <Image src={d1} alt='Card image cap' />
        </a>
      </Link>
    </div>
  )
}

export default DiscountBanner
