import React from 'react'
import brand1 from '../../public/Brand Logo1.png'
import Image from 'next/image'
import Store from '../icons/regular/Store'
import p1 from '../../public/p1.jpg'
import Basket from '../icons/regular/Basket'
import Share from '../icons/regular/Share'
import { Result } from '../../types/productTypes'
import Link from 'next/link'
import { useAppDispatch } from '../../redux/hooks'
import { cartIncrement } from '../../redux/cart/cartSlice'

interface ItemCardProps {
  product: Result
}

const ItemCard = ({ product }: ItemCardProps) => {
  // const thumb_img = JSON.parse(JSON.stringify(product?.thumbnail_image))
  // console.log(product?.thumbnail_image)
  // console.log(thumb_img.map((value: any) => value[0]))
  // if (typeof product?.thumbnail_image == 'string') {
  //   thumb_img = JSON.parse(product?.thumbnail_image)
  // }
  // console.log(thumb_img)
  // const dispatch = useAppDispatch()
  return (
    <>
      <div className='item-card'>
        <div className='thumbnail-wrap'>
          <figure className='thumbnail-img'>
            <Link href={`products/${product?.id}`}>
              <a>
                {/* <Image
                  alt={JSON.parse(JSON.stringify(product?.thumbnail_image))}
                  src={
                    '/' + JSON.parse(JSON.stringify(product?.thumbnail_image))
                  }
                  height={1450}
                  width={1550}
                /> */}
                <Image alt='api' src={p1} />
              </a>
            </Link>
          </figure>

          <div className='item-card__offer__sale text-center'>SALE</div>
          <div className='brand-logo'>
            <Image
              className='item-card__imagecontainer'
              alt='api'
              src={brand1}
              height={38}
              width={38}
            />
          </div>
          <div className='item-card__hover'>
            <button type='button' className='btn btn-secondary'>
              <Share />
            </button>
            <button type='button' className='btn btn-secondary'>
              <Share />
            </button>
          </div>
        </div>
        <div className='item-card-content'>
          <h5>
            <Link href='/products'>
              <a>{product?.name}</a>
            </Link>
          </h5>
          <div className='d-flex justify-content-between p-1'>
            <div className='item-card__shop'>
              <Store /> <span className='ms-2'>{product?.id}</span>
            </div>
            <div className='item-card__rating'>{product?.id}</div>
          </div>
          <div className='d-flex align-items-center text-center justify-content-between pt-3'>
            <div className='item-card__price'>
              <span className='item-card__price-new'>
                {product?.stock?.price}
              </span>
              <span className='item-card__price-old'>
                {product?.stock?.mrp}
              </span>
            </div>
            <button
              className='item-card__icon me-3'
              // onClick={() => dispatch(cartIncrement())}
            >
              <Basket />
            </button>
          </div>
        </div>
      </div>
    </>
  )
}

export default ItemCard
