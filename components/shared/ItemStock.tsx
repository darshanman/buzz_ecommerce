import Link from 'next/link'
import React from 'react'
import Image from 'next/image'

const ItemStock = () => {
  return (
    <div className='col-6 col-lg-3'>
      <Link href='/products'>
        <a>
          <div className='stock pt-3'>
            <h4>Fitness HeadPhones</h4>
            <div className='stock__image d-flex text-center mx-auto'>
              <Image src='/p2.png' alt='Card image cap' layout='fill' />
            </div>
            <h6>10 items</h6>
          </div>
        </a>
      </Link>
    </div>
  )
}

export default ItemStock
