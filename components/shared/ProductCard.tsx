import React from 'react'
import Image from 'next/image'
import brand1 from '../../public/Brand Logo1.png'
import p1 from '../../public/r2.png'
import QuestionCircle from '../icons/regular/QuesCircle'
import ShareNodes from '../icons/regular/ShareNode'
import Link from 'next/link'
import FastTruck from '../icons/light/FastTruck'
import Box from '../icons/light/Box'
import { Tab, Tabs } from 'react-bootstrap'
import FormButton from '../common/FormButton'
import QuantityButton from '../common/QuantityButton'
import WishListButton from '../common/WishListButton'
import { Result } from '../../types/productTypes'

// interface Props {
//   product: Result
// }
const ProductDetail = () =>
  // product: Props
  {
    return (
      <section>
        <div className='row'>
          <div className='col-md-6'>
            <Image
              alt='100x100'
              src={p1}
              data-holder-rendered='true'
              width={550}
              height={550}
            />
            <div className='soldBy py-5'>
              SOLD BY
              <hr></hr>
              <div className='mt-4'>
                <button className='float-end mt-4'>Visit Store</button>
                <h4 className='pt-4 ps-1'>Sherpa Ventures</h4>4 (500)
              </div>
            </div>
          </div>
          <div className='product col-md-6 pe-0'>
            <Image alt='100x100' src={brand1} data-holder-rendered='true' />
            <div className='d-flex text-center align-items-center justify-content-between'>
              <h1 className='mt-2'>The Bean box version 2</h1>
              <WishListButton />
            </div>
            <div className='d-flex text-center product__price align-items-center justify-content-between pt-4'>
              <div className='product__price'>
                <span className='product__price-new'>$6666666</span>
                <span className='product__price-old'>$555555</span>
              </div>
              <div className='product__rating'>4 (60)</div>
            </div>
            <div className='product__container mt-5'>
              Color
              <div className='d-flex flex-row mt-4'>
                <button className='product__container__color'></button>
                <button className='product__container__color'></button>
              </div>
            </div>
            <div className='product__container mt-4 mb-4'>
              Sizes
              <div className='d-flex flex-row mt-4'>
                <button className='product__container__sizes'>s</button>
                <button className='product__container__sizes'>m</button>
              </div>
            </div>
            Available in stock
            <div className='product__container mt-4 mb-4'>
              Quantity
              <div className='d-flex flex-row justify-content-between mt-4'>
                <QuantityButton />
                <button className='product__container__button'>
                  Add to Cart
                </button>
              </div>
              <FormButton name={'Buy Now'} className='mt-3'></FormButton>
            </div>
            <div className='product__description d-flex mb-4'>
              <Link href='/'>
                <a>
                  <QuestionCircle />
                  Ask a Question
                </a>
              </Link>
              <Link href='/'>
                <a>
                  <ShareNodes />
                  Share
                </a>
              </Link>
            </div>
            <hr />
            <p className='pt-3 pb-0'>
              <FastTruck /> &nbsp; Estimated Delivery: <span>3 - 5 days</span>
            </p>
            <p>
              <Box /> &nbsp; Free Shipping & Returns:
              <span> On all orders over $200.00</span>
            </p>
          </div>
        </div>
        <div className='tab'>
          <Tabs
            defaultActiveKey='description'
            id='uncontrolled-tab-example'
            className='mx-5 pt-5'
          >
            <Tab eventKey='description' title='Description'>
              Lorem ipsum dolor sit, amet consectetur adipisicing elit. Voluptas
              sint debitis maiores placeat repudiandae magni sit. Reprehenderit
              cupiditate eaque sapiente ad nulla error officia, eius cum atque
              velit ipsa laudantium.
            </Tab>
            <Tab eventKey='information' title='Additional Information'>
              Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quo id
              quibusdam accusantium aspernatur. Quo aliquam quas perspiciatis,
              velit, optio a earum consectetur dicta, dolorem aut expedita
              blanditiis dolore ducimus tenetur?
            </Tab>
            <Tab eventKey='reviews' title='Reviews (10)'>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis
              maxime, nisi reprehenderit corrupti incidunt praesentium saepe sed
              non, amet aperiam officiis illo quas. Iure veritatis ducimus quam,
              temporibus itaque nesciunt.
            </Tab>
            <Tab eventKey='questions' title='Questions'>
              Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quos
              officia labore id minus odio iusto ipsa veniam sapiente animi
              magnam error, saepe, excepturi ullam cum minima natus consequatur,
              tempore voluptatem?
            </Tab>
          </Tabs>
        </div>
      </section>
    )
  }

export default ProductDetail
