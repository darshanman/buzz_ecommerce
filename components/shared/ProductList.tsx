import React from 'react'
import { Col, Row } from 'react-bootstrap'
import { Result } from 'types/productTypes'
import ItemCard from './ItemCard'

interface ProductProps {
  types: Result[]
}
const ProductList = ({ types }: ProductProps) => {
  return (
    <Row className='g-5'>
      {types?.map((product, key) => {
        return (
          <Col key={key} sm={12} md={4}>
            <ItemCard product={product} />
          </Col>
        )
      })}
    </Row>
  )
}

export default ProductList
