import { ErrorMessage } from 'formik'
import React from 'react'
import Select from 'react-select'
import { checkFormControl, checkFormGroup } from '../../utils/helpers'
import { CustomSelectProps } from '../../types/selectField'

interface Option {
  label: string
  value: string
}

export const CustomSelect = ({
  className,
  error,
  placeholder,
  field,
  form,
  options,
  labelName,
  touch,
  fieldRequired = true,
  isMulti = false,
  ...restProps
}: CustomSelectProps & Partial<HTMLInputElement>) => {
  const onChange = (option: any) => {
    form.setFieldValue(
      field.name,
      isMulti
        ? (option as Option[]).map((item: Option) => item.value)
        : (option as Option).value
    )
  }

  const getValue = () => {
    return isMulti
      ? options.filter(
          (option: { value: any }) => field.value.indexOf(option.value) >= 0
        )
      : options.find((option: { value: any }) => option.value === field.value)
  }

  return (
    <div className={checkFormGroup(error)}>
      {labelName && (
        <label htmlFor={field.name} className='form-label'>
          {labelName} {fieldRequired && <span className='asterisk'>*</span>}
        </label>
      )}
      <Select
        {...restProps}
        className={checkFormControl(error, touch)}
        name={field.name}
        value={getValue()}
        onChange={onChange}
        placeholder={placeholder}
        options={options}
        isMulti={isMulti}
      />
      <ErrorMessage
        name={field.name}
        component='span'
        className='invalid-feedback'
      />
    </div>
  )
}

export default CustomSelect
