const nextConfig = {
  reactStrictMode: true,
};

module.exports = {
  nextConfig,
  reactStrictMode: true,

   env: {
    NEXT_API_BASE_URL: 'http://192.168.0.151:8006/api/v1',
   },

  images: {
    domains: ['fakestoreapi.com'],
  },
}
