import type { AppProps } from 'next/app'
import 'bootstrap/dist/css/bootstrap.min.css'
import '../styles/_bundle.scss'
import { Provider } from 'react-redux'
import store from '../redux/store'
import { SSRProvider } from 'react-bootstrap'

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <Provider store={store}>
        <SSRProvider>
          <Component {...pageProps} />
        </SSRProvider>
      </Provider>
  )
}

export default MyApp
