import http from './httpService';
import jwtDecode from 'jwt-decode';
import urls from 'pages/constant/urls';

const accessTokenKey = 'access';

interface LoginValuesProps {
    username: string;
    password: string;
}
interface JwtToken {
    exp: number;
}

// Get the CurrentUser Detail from the token
export const getCurrentUser = () => {
    try {
        const jwt = localStorage.getItem(accessTokenKey) as string;
        if (jwtDecode<JwtToken>(jwt).exp < Date.now() / 1000) {
            logout();
        } else {
            return jwtDecode(jwt);
        }
    } catch (err) {
        return null;
    }
};

// Logout
export const logout = () => {
    localStorage.removeItem(accessTokenKey);
};

// Get the token-key from the localStorage
export const getJwt = () => localStorage.getItem(accessTokenKey);

const auth = {
    logout,
    getCurrentUser,
    getJwt,
};

export default auth;
