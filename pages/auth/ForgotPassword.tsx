import React from "react";
import LoginLayout from "../../components/auth/LoginLayout";
import ForgetPassword from "../../components/auth/ForgetPassword";

const Forget = () => {
  return (
    <div>
      <LoginLayout
        topLeftText="Already have an account ?"
        topRightText=" Login"
        welcomeText="Hey! No worries   👋"
        headerText="Forgot your password ?"
        link="/auth/Login"
      >
        <ForgetPassword />
      </LoginLayout>
    </div>
  );
};

export default Forget;
