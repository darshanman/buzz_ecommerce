import React from "react";
import LoginLayout from "../../components/auth/LoginLayout";
import Login from "../../components/auth/Login";

const LoginPage = () => {
  

  return (
    <div>
      <LoginLayout
        topLeftText="Don’t have an account ?"
        topRightText="Create an account"
        welcomeText="Welcome Back!   👋"
        headerText="Login to your account"
        link="/auth/Register"
      >
        <Login />
      </LoginLayout>
    </div>
  );
};

export default LoginPage;
