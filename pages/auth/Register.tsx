import React from "react";
import LoginLayout from "../../components/auth/LoginLayout";
import SignUp from "../../components/auth/Signup";

const Register = () => {
  return (
    <div>
      <LoginLayout
        topLeftText="Already have an account ?"
        topRightText=" Login"
        welcomeText="Welcome To Buzz !   👋"
        headerText="Create your account"
        link="/auth/Login"
      >
        <SignUp />
      </LoginLayout>
    </div>
  );
};

export default Register;
