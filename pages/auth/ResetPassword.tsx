import React from "react";
import LoginLayout from "../../components/auth/LoginLayout";
import ResetPassword from "../../components/auth/ResetPassword";

const Reset = () => {
  return (
    <div>
      <LoginLayout
        topLeftText="Remember your password?"
        topRightText=" Login"
        welcomeText="Hey! Nice job   👋"
        headerText="Create new password"
        link="/auth/Login"
      >
        <ResetPassword />
      </LoginLayout>
    </div>
  );
};

export default Reset;
