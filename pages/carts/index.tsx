import React from 'react'
import Layout from '../../components/Layout'
import { ProgressBar } from 'react-bootstrap'
import CartItems from '../../components/cart/CartItems'

const Cart = () => {
  return (
    <Layout>
      <div className='container py-5'>
        <div className='top_section text-center width-auto'>
          <h3 className='text-center mt-5'>Shopping Cart</h3>
          <span>
            Congrats! You are eligible for <strong>FREE Shipping</strong>
          </span>
          <span className='progress-bar'>
            <ProgressBar variant='warning' now={60} />
          </span>
        </div>
        <CartItems />
      </div>
    </Layout>
  )
}

export default Cart
