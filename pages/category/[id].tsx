import Layout from '@components/Layout'
import React from 'react'
import { Col, Row } from 'react-bootstrap'
import ProductList from '@components/shared/ProductList'
import type { GetStaticProps } from 'next'
import http from 'pages/api/httpService'
import { Result } from 'types/productTypes'
// import { BreadCrumb } from '@components/shared/BreadCrumb'

const CategoryList = ({ productsData }: { productsData: Result[] }) => {
  return (
    <section>
      <Layout>
        <div className='container'>
          {/* <BreadCrumb /> */}
          <Row>
            <Col sm={3}>xdsdsds</Col>
            <Col sm={9}>
              <ProductList types={productsData} />
            </Col>
          </Row>
        </div>
      </Layout>
    </section>
  )
}
export const getStaticProps: GetStaticProps = async () => {
  try {
    const { data: productsData } = await http.get(
      'http://192.168.0.151:8006/api/v1/product-generic'
    )
    if (productsData.error) throw new Error(productsData.error.message)
    return {
      props: {
        productsData,
      },
      revalidate: 10,
    }
  } catch (err: any) {
    return {
      props: {
        productsData: [],
      },
      revalidate: 10,
    }
  }
}
export default CategoryList
