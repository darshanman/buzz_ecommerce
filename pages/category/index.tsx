import React from 'react'
import { Col, Row } from 'react-bootstrap'
import type { GetStaticProps } from 'next'

import Layout from '@components/Layout'
import ProductList from '@components/shared/ProductList'
import http from 'pages/api/httpService'
import { Result } from 'types/productTypes'
import AdBanner from '@components/common/AdBanner'
import Grid from '@components/icons/regular/Grid'
import List from '@components/icons/regular/List'
import FilterList from '@components/icons/regular/FilterList'
// import { BreadCrumb } from '@components/shared/BreadCrumb'

const CategoryList = ({ productsData }: { productsData: Result[] }) => {
  return (
    <section>
      <Layout>
        <div className='container'>
          {/* <BreadCrumb /> */}
          <AdBanner />
          <Row>
            <div className='d-flex align-items-center text-center justify-content-between mb-5'>
              <h3>Windows</h3>
              <div className='icon-holder d-flex align-items-center text-center'>
                <button>
                  <Grid />
                </button>
                <button>
                  <List />
                </button>
                <button>
                  <FilterList />
                </button>
                Filter
              </div>
            </div>
            <Col sm={3}>xdsdsds</Col>
            <Col sm={9}>
              <ProductList types={productsData} />
            </Col>
          </Row>
        </div>
      </Layout>
    </section>
  )
}
export const getStaticProps: GetStaticProps = async () => {
  try {
    const { data: productsData } = await http.get(
      'http://192.168.0.151:8006/api/v1/product-generic'
    )
    if (productsData.error) throw new Error(productsData.error.message)
    return {
      props: {
        productsData,
      },
      revalidate: 10,
    }
  } catch (err: any) {
    return {
      props: {
        productsData: [],
      },
      revalidate: 10,
    }
  }
}
export default CategoryList
