import React from 'react'
import Image from 'next/image'
import p1 from '../public/p1.jpg'
import Layout from '../components/Layout'
import BillingForm from '../components/common/BillingForm'
import { Breadcrumb } from 'react-bootstrap'
import OrderSummary from '../components/common/OrderSummary'
import AdBanner from '../components/common/AdBanner'

const checkout = () => {
  return (
    <Layout>
      <Breadcrumb />
      <section>
        <div className='container checkout'>
          <div className='row'>
            <div className='col-12 col-md-6'>
              <BillingForm />
            </div>
            <div className='col-12 col-md-6'>
              <OrderSummary />
            </div>
          </div>
          <AdBanner />
        </div>
      </section>
    </Layout>
  )
}

export default checkout
