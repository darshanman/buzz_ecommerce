const productPath = '/product'
const authPath = '/customer'

const urls = {
    auth: {
        login: `${authPath}/login`,
        signup: `${authPath}/register`
    },
    products: {
        base: `${productPath}`,
        brand: `${productPath}/brand`,
        category: `${productPath}/category`,
        merchant: `${productPath}/merchant`,
    },
    services: {

    },
    carts: {

    },
    user: {

    },
    verify: {
        base: '/verify'
    }
}

export default urls