import BrandDristibute from "@components/BrandDristibute";
import Categories from "@components/Categories";
import AdBanner from "@components/common/AdBanner";
import Discount from "@components/Discount";
import Layout from "@components/Layout";
import Mav from "@components/Mav";
import MultiRow from "@components/MultiRow";
import FeatureSlider from "@components/Slider";
import SpecialDeals from "@components/SpecialDeals";
import { Product, Result } from "types/productTypes";
import type { GetStaticProps } from "next";
import http from "./api/httpService";

const Home = ({ productsData }: { productsData: Result[] }) => {
  return (
    <Layout>
      <>
        <section className="container">
          <FeatureSlider />
          <Discount />
          <Categories />
          <SpecialDeals types={productsData} />
          <MultiRow title="Just For You" types={productsData} />
          <MultiRow title="Hottest Deals" types={productsData} />
          <Mav />
          <MultiRow title="Top Picks" types={productsData} />
          <BrandDristibute />
          <AdBanner />
        </section>
      </>
    </Layout>
  );
};

export const getStaticProps: GetStaticProps = async () => {
  try {
    const { data: productsData } = await http.get(
      "http://172.16.16.31:8011/api/v1/product-generic"
    );
    if (productsData.error) throw new Error(productsData.error.message);
    return {
      props: {
        productsData,
      },
      revalidate: 10,
    };
  } catch (err: any) {
    return {
      props: {
        productsData: [],
      },
      revalidate: 10,
    };
  }
}

export default Home;
