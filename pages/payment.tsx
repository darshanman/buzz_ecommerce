import React from 'react'
import { Breadcrumb } from 'react-bootstrap'
import AdBanner from '../components/common/AdBanner'
import BillingForm from '../components/common/BillingForm'
import OrderSummary from '../components/common/OrderSummary'
import PaymentForm from '../components/common/paymentForm'
import Layout from '../components/Layout'

const Payment = () => {
  return (
    <Layout>
      <Breadcrumb />
      <section>
        <div className='container checkout'>
          <div className='row'>
            <div className='col-12 col-md-6'>
              <PaymentForm />
            </div>
            <div className='col-12 col-md-6'>
              <OrderSummary />
            </div>
          </div>
          <AdBanner />
        </div>
      </section>
    </Layout>
  )
}

export default Payment
