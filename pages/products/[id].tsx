import { GetStaticProps } from 'next'
import AdBanner from '../../components/common/AdBanner'
import Layout from '../../components/Layout'
import MultiRow from '../../components/MultiRow'
import BreadCrumb from '../../components/shared/BreadCrumb'
import ProductCard from '../../components/shared/ProductCard'
import { Result } from '../../types/productTypes'

const ProductDetail = ({ product }: { product: Result[] }) => {
  return (
    <>
      <Layout>
        <BreadCrumb />
        {/* {product?.map((product, key) => {
          return (
            <div key={key} className='col-sm-6 col-lg-3 d-flex'>
              <ProductCard product={product} />
            </div>
          )
        })} */}
        <ProductCard />
        <MultiRow title='Related Products' types={product} />
        <AdBanner />
      </Layout>
    </>
  )
}
export async function getStaticPaths() {
  const res = await fetch('https://fakestoreapi.com/products')
  const Product = await res.json()

  return {
    paths: Product.map((product: { id: string }) => {
      return { params: { id: String(product.id) } }
    }),
    fallback: false,
  }
}

export async function getStaticProps({ params }: { params: { id: string } }) {
  const res = await fetch(`https://fakestoreapi.com/products/1`)
  const product = res.json()

  console.log(product)
  return {
    props: {
      product,
    },
  }
}
export default ProductDetail
