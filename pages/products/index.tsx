import { GetStaticProps } from 'next'
import AdBanner from '../../components/common/AdBanner'
import Layout from '../../components/Layout'
import MultiRow from '../../components/MultiRow'
import BreadCrumb from '../../components/shared/BreadCrumb'
import ProductCard from '../../components/shared/ProductCard'
import { Result } from '../../types/productTypes'

const ProductDetail = ({ product }: { product: Result[] }) => {
  return (
    <>
      <Layout>
        <BreadCrumb />
        <section>
          <div className='container'>
            <ProductCard />
            <MultiRow title='Related Products' types={product} />
            <AdBanner />
          </div>
        </section>
      </Layout>
    </>
  )
}
export const getStaticProps: GetStaticProps = async (context) => {
  const res = await fetch('https://fakestoreapi.com/products?limit=4')
  const Product = await res.json()

  return {
    props: {
      product: Product,
    },
  }
}
export default ProductDetail
