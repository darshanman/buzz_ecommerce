import { NextRouter, useRouter } from 'next/router'
import { useEffect } from 'react'
import http from './api/httpService'
import urls from './constant/urls'

const Verify = () => {
  const { query }: NextRouter = useRouter()
  let userId = query.u
  let token = query.t

  useEffect(() => {
    const verifyEmail = async () => {
      await http.post(`${urls.verify.base}/${userId}/${token}`)
    }
    verifyEmail()
  }, [userId, token])

  return (
    <div>
      You are successfully verified!! <button>Go to home page</button>
    </div>
  )
}

export default Verify
