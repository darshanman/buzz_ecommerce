import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { RootState } from '../store'

type InitialState = {
  numOfCarts: number
}
const initialState: InitialState = {
  numOfCarts: 0
}

const cartSlice = createSlice({
  name: 'cart',
  initialState,
  reducers: {
    cartIncrement: state => {
      state.numOfCarts++
    },
    cartDecrement: state => {
      state.numOfCarts--
    }
  }
})
export const selectCount = (state: RootState) => state.cart

export default cartSlice.reducer
export const { cartIncrement, cartDecrement } = cartSlice.actions