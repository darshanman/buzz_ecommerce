export interface BillingFieldProps {
    first_name: string;
    last_name: string;
    country: string
    state: string;
    city: string;
    street_address: string;
    postal_code: string;
    email: string;
    phone: string;
}
