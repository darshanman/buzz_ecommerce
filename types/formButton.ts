export interface FormButtonProps {
    name: string;
    className?: string;
}
