import { ReactNode } from "react";

export interface MetaDataProps {
    children?:ReactNode;
    title?: string;
    description?: string;
    keywords?: string;
    ogImage?: string;
    ogUrl?: string;
}