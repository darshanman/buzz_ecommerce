export interface Product {
  count?: number;
  next?: null;
  previous?: null;
  results?: Result[];
}

export interface Result {
  id?: number;
  stock?: Stock;
  category?: Category;
  brand?: Brand | null;
  added_by?: string;
  rating?: Rating;
  created_at?: Date;
  updated_at?: Date;
  status?: string;
  is_active?: boolean;
  name?: string;
  type?: string;
  model_no?: null | string;
  thumbnail_image?: string;
  product_status?: string;
  description?: string;
  slug?: string;
  notes?: null | string;
}

export interface Brand {
  id?: number;
  slug?: string;
  name?: string;
  image?: any[];
  banner?: string;
}

export interface Category {
  id?: number;
  name?: string;
  parent?: number;
  slug?: string;
}

export interface Rating {
  rating__avg?: null;
  count?: number;
}

export interface Stock {
  id?: number;
  price?: number;
  mrp?: number;
  slug?: string;
}