export interface ResetPasswordValuesProps{
    password: string;
    confirmPassword: string;
}