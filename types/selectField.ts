import { FieldProps } from "formik"

export interface CustomSelectProps extends FieldProps {
    options: any
    labelName: string
    error?: string;
    isMulti?: boolean
    fieldRequired?: boolean;
    touch?: boolean;
    className?: string
    placeholder?: string
}
