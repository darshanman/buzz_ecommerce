export interface ShipBillingFieldProps {
    ship_first_name: string;
    ship_last_name: string;
    ship_country: string
    ship_state: string;
    ship_city: string;
    ship_street_address: string;
    ship_postal_code: string;
    ship_email: string;
    ship_phone: string;
}
