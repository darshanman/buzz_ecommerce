import { BillingFieldProps } from '../types/billing';
import { ForgetPasswordValuesProps } from '../types/forgetPassword';
import { LoginValuesProps } from '../types/login';
import { ResetPasswordValuesProps } from '../types/resetPassword';
import { ShipBillingFieldProps } from '../types/shipping';
import { SignupValuesProps } from '../types/signup';

// Login page data
export const loginFormData: LoginValuesProps = {
    email: '',
    password: '',
};

export const signupFormData: SignupValuesProps = {
    email: '',
    password: '',
    first_name: '',
    last_name: '',
    phone: '',
    confirm_password: '',
    username: ''
};

export const forgetPasswordFormData: ForgetPasswordValuesProps = {
    email: '',
};

export const resetPasswordFormData: ResetPasswordValuesProps = {
    password: '',
    confirmPassword: ''
}
export const billingFormData: BillingFieldProps = {
    first_name: '',
    last_name: '',
    country: '',
    state: '',
    city: '',
    street_address: '',
    postal_code: '',
    email: '',
    phone: ''
}
export const shippingBillingFormData: ShipBillingFieldProps = {
    ship_first_name: '',
    ship_last_name: '',
    ship_country: '',
    ship_state: '',
    ship_city: '',
    ship_street_address: '',
    ship_postal_code: '',
    ship_email: '',
    ship_phone: ''
}
