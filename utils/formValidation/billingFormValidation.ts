import * as Yup from 'yup';

let emailValidate, firstNameValidate, lastNameValidate, phoneNumberValidate, countryValidate, stateValidate, postalCodeValidate, cityValidate, streetValidate;

firstNameValidate = Yup.string()
    .min(2, 'First Name is too short - should be 2 chars minimum.')
    .required('Required field');
lastNameValidate = Yup.string()
    .min(2, 'Last Name is too short - should be 2 chars minimum.')
    .required('Required field');

emailValidate = Yup.string()
    .email('Invalid email address')
    .required('Required field');

stateValidate = Yup.string()
    .min(2, 'State name is too short - should be 2 chars minimum.')
    .required('Required field');
cityValidate = Yup.string()
    .min(2, 'State name is too short - should be 2 chars minimum.')
    .required('Required field');

streetValidate = Yup.string()
    .min(2, 'State name is too short - should be 2 chars minimum.')
    .required('Required field');

postalCodeValidate = Yup.number()
    .min(2, 'State name is too short - should be 2 chars minimum.')
    .required('Required field');

countryValidate = Yup.string()
    .required('Required field');

phoneNumberValidate = Yup.string()
    .required('Required field')
    .min(9, 'Number is too short - should be 8 chars minimum.')
    .max(11, 'Number is too long - should be 11 chars maximum.')

const billingFormSchema = Yup.object().shape({
    first_name: firstNameValidate,
    last_name: lastNameValidate,
    country: countryValidate,
    email: emailValidate,
    state: stateValidate,
    city: cityValidate,
    postal_code: postalCodeValidate,
    street_address: streetValidate,
    phone: phoneNumberValidate,
});

export default billingFormSchema;
