import * as Yup from 'yup';

let emailValidate;

emailValidate = Yup.string()
    .email('Invalid email address')
    .required('Required field');

const forgetPasswordFormSchema = Yup.object().shape({
    email: emailValidate,
});

export default forgetPasswordFormSchema;
