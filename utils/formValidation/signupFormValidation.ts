import * as Yup from 'yup';

let userNameValidate, firstNameValidate, lastNameValidate, emailValidate, phoneNumberValidate, passwordValidate, confirmPasswordValidate;

userNameValidate = Yup.string()
    .min(6, 'Username is too short - should be 6 chars minimum.')
    .required('Required field');
firstNameValidate = Yup.string()
    .min(2, 'First Name is too short - should be 2 chars minimum.')
    .required('Required field');
lastNameValidate = Yup.string()
    .min(2, 'Last Name is too short - should be 2 chars minimum.')
    .required('Required field');
emailValidate = Yup.string()
    .email('Invalid email address')
    .required('Required field');
phoneNumberValidate = Yup.string()
    .required('Required field')
    .min(9, 'Number is too short - should be 8 chars minimum.')
    .max(11, 'Number is too long - should be 11 chars maximum.')
passwordValidate = Yup.string()
    .required('Required field')
    .min(8, 'Password is too short - should be 8 chars minimum.')
    .matches(/[a-zA-Z]/, 'Password can only contain Latin letters.');
confirmPasswordValidate = Yup.string()
    .required('Required field')
    .oneOf([Yup.ref('password'), null], 'Password must match')

const signupFormSchema = Yup.object().shape({
    username: userNameValidate,
    first_name: firstNameValidate,
    last_name: lastNameValidate,
    email: emailValidate,
    phone: phoneNumberValidate,
    password: passwordValidate,
    confirm_password: confirmPasswordValidate,
});

export default signupFormSchema;
